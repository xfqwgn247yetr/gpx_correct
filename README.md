- 解決行車器錄器沒有影像檔無法觀看軌跡以及修正軌跡數據異常。
- 輸出純軌跡圖&地形軌跡圖
---
Python 3.7.3

# 安裝相關套件
```pip -r requirements.txt```
---
```
.
├── gpx
│   ├── FRON0618.GPX
│   ├── FRON0619.GPX
│   ⁝
├── GPX_Parse.py 
├── README.md
└── requirements.txt
```

# 相關參數

```
-P PATH, --PATH PATH  輸入GPS資料夾路徑 

-M {1,2}, --Mode {1,2}
    地圖顯示模式
        1: 一般地形模式
        2: 灰底模式

-D {1,2}, --Dual {1,2}
    界面顯示模式
        1.單地圖顯示
        2.雙地圖顯示
```