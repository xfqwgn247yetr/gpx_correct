#!/usr/bin/python
# -*- coding: utf-8 -*-
import io
import os
import sys
import time
import folium
import argparse
import subprocess
import numpy as np
import webbrowser as wb
import xml.etree.ElementTree as ET

from PIL import Image
from os.path import join
from folium import plugins
from os.path import getsize
from selenium import webdriver
from pyroutelib3 import Router
from argparse import RawTextHelpFormatter

#---Fixed Bug Data---
def Data_correction(file_name, all_point):
    #---Defind---
    ET.register_namespace('', "http://www.topografix.com/GPX/1/1")
    ET.register_namespace(' ', "http://www.topografix.com/GPX/1/0")

    namespace = {'gpx':'http://www.topografix.com/GPX/1/1}trk'}
    
    tree = ET.parse(file_name)
    root = tree.getroot()
    
    for elem in root:
        for item in range(len(elem[1])):
            tmp_router = []
            #---lat---
            lat_float1 = float(elem[1][item].attrib['lat'])
            lat_float2 = float(elem[1][item - 1].attrib['lat'])
            if lat_float1 != lat_float2 and item != 0:
                lat_gap = format(lat_float1 - lat_float2, '.7f')
                
                if(abs(float(lat_gap)) > 0.001):
                    elem[1][item].attrib['lat'] = elem[1][item - 1].attrib['lat']

                    if(elem[1][item].attrib['lon'].split('.')[0] == elem[1][item - 1].attrib['lat'].split('.')[0]):
                        elem[1][item].attrib['lat'] = elem[1][item].attrib['lon']
            tmp_router.append(lat_float1)
                    # tree.write(file_name, encoding = 'utf-8', xml_declaration = True)
            #---lon---
            lon_float1 = float(elem[1][item].attrib['lon'])
            lon_float2 = float(elem[1][item - 1].attrib['lon'])
            if lon_float1 != lon_float2 and item != 0:
                lon_gap = format(lon_float1 - lon_float2, '.7f')

                if(abs(float(lon_gap)) > 0.001):
                    elem[1][item].attrib['lon'] = elem[1][item - 1].attrib['lon']  

                    # tree.write(file_name, encoding = 'utf-8', xml_declaration = True)
            tmp_router.append(lon_float1)

            #---Time---
            if len(elem[1][item][1].text) != 20:
                print(elem[1][item][1].text)
                elem[1][item][1].text = elem[1][item - 1][1].text

            all_point.append(tmp_router)
    tree.write(file_name, encoding = 'utf-8', xml_declaration = True)
    return all_point
    
#---Calculation Center Point---
def Calculation_Center_Point(list_point):
    #---Map Center Point---
    min_point = min(list_point)
    max_point = max(list_point)
    center_dis = list(map(lambda x : abs(x[0] - x[1]), zip(max_point, min_point)))
    min_x = min(min_point[0], max_point[0])
    min_y = min(min_point[1], max_point[1])
    center_point = [(center_dis[0] / 2) + min_x, center_dis[1] / 2 + min_y]

    return center_point

#---GPS Map Mode---
def Choose_Map(all_point, center_point, mode, dual):
    Gray_background = r'http://{s}.tiles.yourtiles.com/{z}/{x}/{y}.png'
    OSM = "openstreetmap"
    attr = "None"

    if dual == 1:
        if mode == 2:
            gpx_map = folium.Map(location = center_point, width = '100%', height = '100%', left = '0%', top = '0%', min_zoom = 0, max_zoom = 18, zoom_start = 12, tiles = Gray_background, attr = attr)# Map Center Point
            Daw_Map(gpx_map, all_point, center_point)
        else:
            gpx_map = folium.Map(location = center_point, width = '100%', height = '100%', left = '0%', top = '0%', min_zoom = 0, max_zoom = 18, zoom_start = 12, tiles = OSM)# Map Center Point
            Daw_Map(gpx_map, all_point, center_point)
    else:
        gpx_map = plugins.DualMap(location = center_point, max_zoom = 18, zoom_start = 12, tiles = Gray_background, attr = attr)# Map Center Point
        folium.TileLayer(OSM).add_to(gpx_map.m2)
        Daw_Map(gpx_map, all_point, center_point)

#---Draw Singl Map---
def Daw_Map(gpx_map, all_point, center_point):    
    #---Maker---
    start_maker = all_point[0]
    end_maker = all_point[len(center_point) - 1]

    gpx_map.add_child(folium.Marker(location = start_maker, popup = "Start"))
    gpx_map.add_child(folium.Marker(location = end_maker, popup = "End"))

    #---Locate---
    plugins.LocateControl().add_to(gpx_map)
    plugins.LocateControl(auto_start = True).add_to(gpx_map)

    #---Draw Map---
    route = folium.PolyLine(
        all_point,
        weight = 8,
        color = "black",
        opactiy = 0.7
    ).add_to(gpx_map)  

    gpx_map.save(os.path.join(r'./', 'Map.html'))

    outdir = "" # This directory has to exist..
    url = "file://{}/Map.html".format(os.getcwd())
    outfn = os.path.join(outdir,"outfig.png")
    subprocess.check_call(["cutycapt","--url={}".format(url), "--out={}".format(outfn), "--min-width=1920", "--min-height=1080"])

#----Main----
try:
    parser = argparse.ArgumentParser(description = "Creat GPS Path", formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument("-P", "--PATH",
                        required = True,
                        type = str,
                        help = 'GPS FILED PATH: ')
    parser.add_argument("-M", "--Mode",
                        type = int,
                        default = 1,
                        choices = range(1, 3),
                        help = 'Please enter number with chooese Map Mode: \n\
                                1: Normal Background Mode \n\
                                2: Gray Background Mode')
    parser.add_argument("-D", "--Dual",
                        type = int,
                        default = 1,
                        choices = range(1, 3),
                        help = 'Chooese View Map Mode: \n\
                                1.Normal Map Mode \n\
                                2.Dual Map Mode')

    args = parser.parse_args()
    gpx_path = args.PATH.strip().replace("'", "") + "/"
    mode = args.Mode
    dual = args.Dual
    # gpx_path = input(r"Input GPS PATH: ").strip().replace("'", "") + "/"
    all_point = []

    for root, dirs, files in os.walk(gpx_path):
        files.sort()
        for file in files:
            fullpath = join(gpx_path, file)
            if getsize(fullpath) < 344:
                try:
                    os.remove(fullpath)
                except OSError as e:
                    print(e)
                else:
                    print("File is deleted successfully")
            else:
                all_point = Data_correction(fullpath, all_point)
    center_point = Calculation_Center_Point(all_point)
    Choose_Map(all_point, center_point, mode, dual)

except(ValueError):
    print("Please enter CORRECT PATH, OK?")
except (EOFError, KeyboardInterrupt):
    print('User Interrupt')